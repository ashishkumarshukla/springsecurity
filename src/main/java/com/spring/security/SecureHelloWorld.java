package com.spring.security;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecureHelloWorld {
	
	@GetMapping("/")
	  public String index() {
	    return "Welcome to the home page!";
	  }

}
